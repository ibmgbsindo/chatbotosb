xquery version "2004-draft";
(:: pragma bea:local-element-parameter parameter="$transactions1" type="ns0:getTransactionResponse1/Transactions" location="../SSPTransactionHandler/TransactionHandler.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:getTransactionResponse1" location="../SSPTransactionHandler/TransactionHandler.wsdl" ::)

declare namespace ns0 = "http://www.example.org/TransactionHandler/";
declare namespace xf = "http://tempuri.org/PULLHandler/ShortElementNew/";

declare function xf:ShortElementNew($transactions1 as element())
    as element(ns0:getTransactionResponse1) {
        <ns0:getTransactionResponse1>
            {
                let $Transactions := $transactions1
                return
                    <Transactions>
                        {
                            for $Transaction in $Transactions/Transaction order by $Transaction/startDate/text() descending
                            return
                                <Transaction>
                                    {
                                        for $trxId in $Transaction/trxId
                                        return
                                            <trxId>{ data($trxId) }</trxId>
                                    }
                                    {
                                        for $msisdn in $Transaction/msisdn
                                        return
                                            <msisdn>{ data($msisdn) }</msisdn>
                                    }
                                    {
                                        for $startDate in $Transaction/startDate 
                                        return
                                            <startDate>{ data($startDate) }</startDate>
                                    }
                                    {
                                        for $endDate in $Transaction/endDate
                                        return
                                            <endDate>{ data($endDate) }</endDate>
                                    }
                                    {
                                        for $status in $Transaction/status
                                        return
                                            if (data($status) = '0') then
                                            <status>Success</status>
                                            else if (data($status) = '99') then
                                            <status>Failed</status>
                                            else
                                            <status>{data($status)}</status>
                                    }
                                    {
                                        for $smsText in $Transaction/smsText
                                        return
                                            <smsText>{ data($smsText) }</smsText>
                                    }
                                    {
                                        for $channel in $Transaction/channel
                                        return
                                            <channel>{ data($channel) }</channel>
                                    }
                                    {
                                        for $remark in $Transaction/remark
                                        return
                                            <remark>{ data($remark) }</remark>
                                    }
                                </Transaction>
                        }
                    </Transactions>
            }
        </ns0:getTransactionResponse1>
};

declare variable $transactions1 as element() external;

xf:ShortElementNew($transactions1)