xquery version "2004-draft";
(:: pragma bea:global-element-parameter parameter="$viewSubscriberResponse1" element="ns2:viewSubscriberResponse" location="UPCCService.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:viewSubscriberResponse" location="../../LDAPHandler/SAPCService.wsdl" ::)

declare namespace ns2 = "http://com/nsn/adapter/upcc";
declare namespace ns1 = "http://com/nsn/adapter/sapc";
declare namespace ns0 = "java:com.nsn.adapter.sapc";
declare namespace xf = "http://tempuri.org/BroadbandHandler/UPCC/test/";

declare function xf:test($viewSubscriberResponse1 as element(ns2:viewSubscriberResponse))
    as element(ns1:viewSubscriberResponse) {
        <ns1:viewSubscriberResponse>
            <ns1:return>
                <ns0:Dn>{ data($viewSubscriberResponse1/ns2:return/ns0:Dn) }</ns0:Dn>
                <ns0:EPC_BlacklistServices>{ data($viewSubscriberResponse1/ns2:return/ns0:EPC_BlacklistServices) }</ns0:EPC_BlacklistServices>
                <ns0:EPC_GroupIds>{ data($viewSubscriberResponse1/ns2:return/ns0:EPC_GroupIds) }</ns0:EPC_GroupIds>
                <ns0:EPC_NotificationData>{ data($viewSubscriberResponse1/ns2:return/ns0:EPC_NotificationData[1]) }</ns0:EPC_NotificationData>
                <ns0:EPC_OperatorSpecificInfo>{ data($viewSubscriberResponse1/ns2:return/ns0:EPC_OperatorSpecificInfo) }</ns0:EPC_OperatorSpecificInfo>
                <ns0:EPC_SubscribedServices>{ data($viewSubscriberResponse1/ns2:return/ns0:EPC_SubscribedServices) }</ns0:EPC_SubscribedServices>
                <ns0:EPC_SubscriberId>{ data($viewSubscriberResponse1/ns2:return/ns0:EPC_SubscriberId) }</ns0:EPC_SubscriberId>
                <ns0:GroupId>{ data($viewSubscriberResponse1/ns2:return/ns0:GroupId) }</ns0:GroupId>
                {
                    for $ObjectClass in $viewSubscriberResponse1/ns2:return/ns0:ObjectClass
                    return
                        <ns0:ObjectClass>{ data($ObjectClass) }</ns0:ObjectClass>
                }
                <ns0:OwnerId>{ data($viewSubscriberResponse1/ns2:return/ns0:OwnerId) }</ns0:OwnerId>
                <ns0:Parent>{ data($viewSubscriberResponse1/ns2:return/ns0:Parent) }</ns0:Parent>
                <ns0:Permissions>{ data($viewSubscriberResponse1/ns2:return/ns0:Permissions) }</ns0:Permissions>
                <ns0:Result>{ data($viewSubscriberResponse1/ns2:return/ns0:Result) }</ns0:Result>
                <ns0:ShareTree>{ data($viewSubscriberResponse1/ns2:return/ns0:ShareTree) }</ns0:ShareTree>
            </ns1:return>
        </ns1:viewSubscriberResponse>
};

declare variable $viewSubscriberResponse1 as element(ns2:viewSubscriberResponse) external;

xf:test($viewSubscriberResponse1)