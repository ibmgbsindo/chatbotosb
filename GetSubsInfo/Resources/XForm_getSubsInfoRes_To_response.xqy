xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace get="http://www.example.org/GetSubsInfo/";
(:: import schema at "GetSubsInfo.wsdl" ::)

declare variable $getSubsInfoResponse as element() (:: schema-element(get:GetSubsInfoResponse) ::) external;
declare variable $simType as xs:string external;

declare function local:func($getSubsInfoResponse as element() (:: schema-element(get:GetSubsInfoResponse) ::), $simType as xs:string) as element() (:: schema-element(get:GetSubsInfoResponse) ::) {
    <get:GetSubsInfoResponse>
        <Tid>{fn:data($getSubsInfoResponse/Tid)}</Tid>
        <Msisdn>{fn:data($getSubsInfoResponse/Msisdn)}</Msisdn>
        <IMSI>{fn:data($getSubsInfoResponse/IMSI)}</IMSI>
        <SimType>{$simType}</SimType>
        <Offers>
            {
                for $Offer in $getSubsInfoResponse/Offers/Offer
                return $Offer
            }
        </Offers>
        <ServiceClass>{fn:data($getSubsInfoResponse/ServiceClass)}</ServiceClass>
        {
            if ($getSubsInfoResponse/CustBalanceInfo)
            then <CustBalanceInfo>{fn:data($getSubsInfoResponse/CustBalanceInfo)}</CustBalanceInfo>
            else ()
        }
        {
            if ($getSubsInfoResponse/CustBillInfo)
            then 
                <CustBillInfo>
                    <MSISDN>{fn:data($getSubsInfoResponse/CustBillInfo/MSISDN)}</MSISDN>
                    <Payment_Due_Date>{fn:data($getSubsInfoResponse/CustBillInfo/Payment_Due_Date)}</Payment_Due_Date>
                    <Pulsa>{fn:data($getSubsInfoResponse/CustBillInfo/Pulsa)}</Pulsa>
                    <Invoice_Date>{fn:data($getSubsInfoResponse/CustBillInfo/Invoice_Date)}</Invoice_Date>
                    <Tagihan_SKR>{fn:data($getSubsInfoResponse/CustBillInfo/Tagihan_SKR)}</Tagihan_SKR>
                    <Total_Tagihan>{fn:data($getSubsInfoResponse/CustBillInfo/Total_Tagihan)}</Total_Tagihan>
                    <SAMU_Flag>{fn:data($getSubsInfoResponse/CustBillInfo/SAMU_Flag)}</SAMU_Flag>
                    <Tagihan_Asset>{fn:data($getSubsInfoResponse/CustBillInfo/Tagihan_Asset)}</Tagihan_Asset>
                    {
                        if ($getSubsInfoResponse/CustBillInfo/RemainingBalance)
                        then <RemainingBalance>{fn:data($getSubsInfoResponse/CustBillInfo/RemainingBalance)}</RemainingBalance>
                        else ()
                    }
                    {
                        if ($getSubsInfoResponse/CustBillInfo/CreditLimit)
                        then <CreditLimit>{fn:data($getSubsInfoResponse/CustBillInfo/CreditLimit)}</CreditLimit>
                        else ()
                    }
                </CustBillInfo>
            else ()
        }
        {
            if ($getSubsInfoResponse/Installments)
            then 
                if (fn:data($getSubsInfoResponse/Installments/InstallmentInfo/Type)!='')
                then
                  <Installments>
                    <InstallmentInfo>
                      <Type>{fn:data($getSubsInfoResponse/Installments/InstallmentInfo/Type)}</Type>
                      <StartDate>{fn:data($getSubsInfoResponse/Installments/InstallmentInfo/StartDate)}</StartDate>
                      <EndDate>{fn:data($getSubsInfoResponse/Installments/InstallmentInfo/EndDate)}</EndDate>
                      <TotalMonth>{fn:data($getSubsInfoResponse/Installments/InstallmentInfo/TotalMonth)}</TotalMonth>
                      <BilledMonth>{fn:data($getSubsInfoResponse/Installments/InstallmentInfo/BilledMonth)}</BilledMonth>
                    </InstallmentInfo>
                    </Installments>
                  else 
                    <Installments/>
            else  ()
        }        
        <SubsType>{fn:data($getSubsInfoResponse/SubsType)}</SubsType>
        <ExpiredDate>{fn:data($getSubsInfoResponse/ExpiredDate)}</ExpiredDate>
        <TerminateDate>{fn:data($getSubsInfoResponse/TerminateDate)}</TerminateDate>
        <Services>
            {
                for $Service in $getSubsInfoResponse/Services/Service
                return 
                <Service>
                    <ServiceType>{fn:data($Service/ServiceType)}</ServiceType>
                    <ServiceName>{fn:data($Service/ServiceName)}</ServiceName>
                    <ServiceDescription>{fn:data($Service/ServiceDescription)}</ServiceDescription>
                    <RegistrationKeyword>{fn:data($Service/RegistrationKeyword)}</RegistrationKeyword>
                    <ShortCode>{fn:data($Service/ShortCode)}</ShortCode>
                    <PackageCode>{fn:data($Service/PackageCode)}</PackageCode>
                    <PackageName>{fn:data($Service/PackageName)}</PackageName>
                    <StartDate>{fn:data($Service/StartDate)}</StartDate>
                    <EndDate>{fn:data($Service/EndDate)}</EndDate>
                    <PackagePeriod>{fn:data($Service/PackagePeriod)}</PackagePeriod>
                    <PeriodUnit>{fn:data($Service/PeriodUnit)}</PeriodUnit>
                    <CycleCount>{fn:data($Service/CycleCount)}</CycleCount>
                    <Quotas>
                        {
                            for $Quota in $Service/Quotas/Quota
                            return 
                            <Quota>
                                <Name>{fn:data($Quota/Name)}</Name>
                                <Description>{fn:data($Quota/Description)}</Description>
                                <RawInitialQuota>{fn:data($Quota/RawInitialQuota)}</RawInitialQuota>
                                <RawAditionalQuota>{fn:data($Quota/RawAditionalQuota)}</RawAditionalQuota>
                                <RawUsedQuota>{fn:data($Quota/RawUsedQuota)}</RawUsedQuota>
                                <RawRemainingQuota>{fn:data($Quota/RawRemainingQuota)}</RawRemainingQuota>
                                <InitialQuota>{fn:data($Quota/InitialQuota)}</InitialQuota>
                                <AdditionalQuota>{fn:data($Quota/AdditionalQuota)}</AdditionalQuota>
                                <UsedQuota>{fn:data($Quota/UsedQuota)}</UsedQuota>
                                <RemainingQuota>{fn:data($Quota/RemainingQuota)}</RemainingQuota>
                                <QuotaUnit>{fn:data($Quota/QuotaUnit)}</QuotaUnit>
                                <BenefitType>{fn:data($Quota/BenefitType)}</BenefitType>
                                <ExpiryDate>{fn:data($Quota/ExpiryDate)}</ExpiryDate>
                                <QuotaSource>{fn:data($Quota/QuotaSource)}</QuotaSource>
                                <Show>{fn:data($Quota/Show)}</Show>
                            </Quota>
                        }
                    </Quotas>
                </Service>
            }
        </Services>
    </get:GetSubsInfoResponse>
};

local:func($getSubsInfoResponse,$simType)