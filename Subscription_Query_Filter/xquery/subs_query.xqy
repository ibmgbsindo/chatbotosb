xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.oracle.com/subscription/data";
(:: import schema at "../xsd/ngssp_subscription.xsd" ::)

declare variable $req as element() (:: schema-element(ns1:ngssp_subscription_query_filter_req) ::) external;

declare function local:func($req as element() (:: schema-element(ns1:ngssp_subscription_query_filter_req) ::)) as element() (:: schema-element(ns1:ngssp_subscription_query_filter_req) ::) {
    <ns1:ngssp_subscription_query_filter_req>
        <ns1:msisdn>{fn:data($req/ns1:msisdn)}</ns1:msisdn>
        {
            if ($req/ns1:transid)
            then <ns1:transid>{fn:data($req/ns1:transid)}</ns1:transid>
            else ()
        }
         {for $a in fn:tokenize(fn:data($req/ns1:status),',')
        return <ns1:status>{$a}</ns1:status>}
    </ns1:ngssp_subscription_query_filter_req>
};

local:func($req)